# BAS

Basic TUI text editor under development made in C that has only one dependency: ncurses.

Right now there are two modes: _normal_ and _insertion_.

In _normal_ mode you can navigate the file using hjkl keys, save the file pressing 's', open a new one pressing 'o' and enter _insertion mode_ pressing 'i'.

In _insertion_ mode you can insert and remove text from the file, as well as go back to _normal mode_ by pressing ESC.

I don't know if it will work on Windows, but on Unix-like systems you simply
```sh
make bas
```
Inside the project folder and then run it with
```sh
./bas
```
